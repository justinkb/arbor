# Copyright 2007 Bryan Østergaard
# Copyright 2008 Richard Brown
# Copyright 2009 Bo Ørsted Andresen
# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="System call trace utility"

HOMEPAGE="https://strace.io"
DOWNLOADS="${HOMEPAGE}/files/${PV}/${PNV}.tar.xz"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freshcode:${PN}"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/files/${PV}/README-${PV}.txt"

LICENCES="
    GPL-2 [[ note = [ test suite ] ]]
    LGPL-2.1
"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    providers: elfutils [[ description = [ Use libdw from elfutils as unwinder provider ] ]]

    ( providers: elfutils libunwind ) [[ number-selected = exactly-one ]]
"

# sydbox makes the tests fail even with all the disabled stuff below and
# RESTRICT="userpriv" due to the usage of ptrace. Last checked: 4.11
RESTRICT="test"

DEPENDENCIES="
    build+run:
        providers:elfutils? ( dev-util/elfutils )
        providers:libunwind? ( dev-libs/libunwind )
    test:
        sys-apps/busybox
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    CC_FOR_BUILD=$(exhost --build)-cc
    --enable-stacktrace
)

if [[ $(exhost --target) == "aarch64"* ]] ; then
    # mpers is not supported by armv8
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --disable-mpers
    )
else
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --enable-mpers=yes
    )
fi

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'providers:elfutils libdw'
    'providers:libunwind'
)

DEFAULT_SRC_COMPILE_PARAMS=( AR=${AR} )

src_test() {
    esandbox disable
    esandbox disable_exec
    emake -j1 check
    esandbox enable
    esandbox enable_exec
}

