# Copyright 2018 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v3
# cargo
# Based in part upon 'cargo.exlib', which is:
#     Copyright 2016 Marc-Antoine Perennou
# Based in part upon 'python.exlib', which is:
#     Copyright 2015 Pierre Lejeune

# Use dependencies from vendor folder (do not fetch using go get)
# If there is a vendor folder in the project root and it contains packages
# hosts (like github.com, gitlab.com, etc), set this to true
myexparam -b use_vendor=false
# Package repository
myexparam project=github.com/user/${PN}

export_exlib_phases src_compile src_install src_test

DEPENDENCIES="
    build:
        dev-lang/go
"

exparam -v PROJECT project

# For last version without a vendor folder, use `go get` to fetch
if ever is_scm && ! exparam -b use_vendor ; then
    export_exlib_phases src_fetch_extra

    export GOPATH="${FETCHEDDIR}/gopath"

    go_src_fetch_extra() {
        scm_src_fetch_extra
        esandbox disable_net
        # -d : do not install the dependencies
        # -u : get and update dependencies
        # -t : download the dependencies required to build the tests
        edo go get -d -u -t ${PROJECT}
        esandbox enable_net
    }
else
    export GOPATH=${WORK}

    if exparam -b use_vendor; then
        export_exlib_phases src_prepare

        go_src_prepare() {
            default
            if [[ -d vendor ]]; then
                edo mv vendor src
                # Some projects reference themselves and need to be in the GOPATH
                edo ln -s ${WORK} src/${PROJECT}
            fi
        }
    fi
fi

go_src_compile() {
    edo go build -o ${PN}
}

go_src_install() {
    emagicdocs
    dobin ${PN}
}

go_src_test() {
   edo go test
}

